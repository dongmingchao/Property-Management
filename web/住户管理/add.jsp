<%@ page contentType="application/json;charset=UTF-8" language="java" %>
<%@ page import="javax.servlet.http.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<fmt:requestEncoding value="UTF8" />
<sql:setDataSource var="snapshot" driver="com.mysql.jdbc.Driver"
                   url="jdbc:mysql://dedsec.club:3306/xiaoqu?useUnicode=true&characterEncoding=utf-8"
                   user="root" password="xiaoqu@1"/>

<c:catch var="error">
<sql:update dataSource="${snapshot}" var="result">
insert into ownerManage_Owner set ownerName = ?, ownerID = ?, birthday = ?, nativePlace = ?, sex = ?, postcode = ?, idNumber = ?, tel = ?<sql:param value="${sqlParam.ownerName}"/><sql:param value="${sqlParam.ownerID}"/><sql:param value="${sqlParam.birthday}"/><sql:param value="${sqlParam.nativePlace}"/><sql:param value="${sqlParam.sex}"/><sql:param value="${sqlParam.postcode}"/><sql:param value="${sqlParam.idNumber}"/><sql:param value="${sqlParam.tel}"/><c:if test="${sqlParam.workPlace!=null}">,workPlace = ?<sql:param value="${sqlParam.workPlace}"/></c:if><c:if test="${sqlParam.address!=null}">, address = ?<sql:param value="${sqlParam.address}"/></c:if>;
</sql:update>
<sql:update dataSource="${snapshot}" var="result">
insert into ownerManage_Bank set ownerName = ?, bank = ?, bankAccount = ?<sql:param value="${sqlParam.ownerName}"/><sql:param value="${sqlParam.bank}"/><sql:param value="${sqlParam.bankAccount}"/>;
</sql:update>
<sql:update dataSource="${snapshot}" var="result">
insert into ownerManage_Time set ownerName = ?, checkinTime = ?, outTime = ?<sql:param value="${sqlParam.ownerName}"/><sql:param value="${sqlParam.checkinTime}"/><sql:param value="${sqlParam.outTime}"/>;
</sql:update>
</c:catch>


<c:choose>
<c:when test='${error!=null}'>
<c:set var="error" value="${error}"/>
<%
    String err = pageContext.getAttribute("error").toString();
    String[] part = err.split(":");
    StringBuilder res = new StringBuilder();
    for(int i = 2; i < part.length; i++) {
        res.append(part[i]).append("|");
    }
%>
<%="{\"status\":\""+res+"\"}"%>
</c:when>
<c:otherwise>{"status":"ok"}</c:otherwise>
</c:choose>
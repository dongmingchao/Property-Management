<%@ page contentType="application/json;charset=UTF-8" language="java" %>
<%@ page import="javax.servlet.http.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<sql:setDataSource var="snapshot" driver="com.mysql.jdbc.Driver"
                   url="jdbc:mysql://dedsec.club:3306/xiaoqu?useUnicode=true&characterEncoding=utf-8"
                   user="root" password="xiaoqu@1"/>

<fmt:parseNumber value="${param.page_no}" var="pageNumber"/>
<sql:query dataSource="${snapshot}" var="result">
SELECT carNum,carAddress, ownerName, serverAddress, usingDate, remark from carManagement <c:if test="${param.carOwnerId!=null}"> where ownerName = ? <sql:param value="${param.carOwnerId}"/></c:if> limit ?, 5;
<sql:param value="${pageNumber}"/>
</sql:query>
[
<c:forEach var="row" items="${result.rows}" varStatus="stat">
{
"车位编号": "<c:out value="${row.carNum}"/>",
"车位位置": "<c:out value="${row.carAddress}"/>",
"停车住户": "<c:out value="${row.ownerName}"/>",
"物业地址": "<c:out value="${row.serverAddress}"/>",
"使用日期": "<c:out value="${row.usingDate}"/>",
"备注": "<c:out value="${row.remark}"/>"
}<c:if test="${!stat.last}">,</c:if>
</c:forEach>
]
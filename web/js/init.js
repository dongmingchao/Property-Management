var ctrl = {};
layui.use('laydate', function () {
    let laydate = layui.laydate;
    ctrl.laydate = laydate;

    laydate.render({
        elem: '#dateFrom',
        range: '~',
        type: 'month'
    });
});
layui.use('element', function () {
    let element = layui.element;

});
layui.use('form', function () {
    let form = layui.form;
    ctrl.form = form;
    form.on('checkbox', function (data) {
        setTimeout(function () {
            chosedRows = document.querySelectorAll('.chosen-row');
        });
    });
    // checkLogin();
});

layui.use('layer', function () {
    let layer = layui.layer;
    ctrl.layer = layer;
    ctrl.loading = layer.load();
});

layui.use('form', function () {
    let form = layui.form;
    form.on('checkbox(choseAll)', function (data) {
        console.log(data);
        let table = document.getElementById('show');
        if (data.othis[0].classList.contains('layui-form-checked')) {
            for (let tr of table.childNodes) {
                tr.classList.add('chosen-row');
                tr.firstChild.childNodes[1].classList.add('layui-form-checked');
            }
        } else for (let tr of table.childNodes) {
            tr.classList.remove('chosen-row');
            tr.firstChild.childNodes[1].classList.remove('layui-form-checked');
        }
    })
});

async function checkLogin(title) {
    let res = {};
    await fetch('/check.jsp?account='+window.parent.cookie.get('account')+'&passwd='+window.parent.cookie.get('passwd')).then(d => d.json()).then(d => {
        if (d[0].auth[title]==='true') res.admin = true;
        if (d[0].see[title] === 'true') res.see = true;
    });
    return res;
}
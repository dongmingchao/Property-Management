/**
 * 将修改同步到数据库
 * @param {HTMLElement} button
 * @param {Array} args 参数数组
 */
function confirm(args) {
    console.log('args', args.data);
    let loading;
    for (let arg of args) {
        if (arg.method === 'add') {
            let submit = document.getElementById(arg.row);
            submit.click();
            if (!arg.finish) continue;
            loading = ctrl.layer.load();
        } else if (arg.method === 'delete') {
            console.log(arg.data.id);
            loading = ctrl.layer.load();
            tasks.push(new Promise(resolve => {
                fetch('delete.jsp?id=' + arg.data.id).then(d => d.json()).then(d => {
                    submit_args.splice(submit_args.indexOf(arg), 1);
                    resolve(d);
                });
            }));
        }else if (arg.method === 'edit') {
            console.log(submit_args);
            let submit = document.getElementById(arg.row);
            submit.click();
            if (!arg.finish) continue;
            loading = ctrl.layer.load();
        }
    }
    Promise.all(tasks).then((d) => {
        console.log('result status',d);
        ctrl.layer.close(loading);
        let msg = "";
        for(let i=0;i<d.length;i++){
            msg+='第'+i+'条请求结果是：'+d[i].status+'<br>';
        }
        ctrl.layer.msg(msg);
        tasks = [];
        getNext(page_no,table);
    }).catch(console.log);
}

let tasks = [];
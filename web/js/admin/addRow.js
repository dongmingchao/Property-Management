function addRow(table) {
    let row_number = table.children.length;
    let line = document.createElement('tr');
    line.classList.add('layui-form');
    line.id = row_number;
    let arg = {method: 'add', data: {}};
    for (let each of admin_table) {
        let td = document.createElement('td');
        td.classList.add('layui-form-item');
        generateTD(each,td,line,arg);
        // switch (each.type) {
        //     case 'checkbox': {
        //         td.innerHTML = '<input type="checkbox" lay-skin="primary" value="1">';
        //     }
        //         break;
        //     // case 'functions':{
        //     //     let item = {};
        //     //     for(let param of each.dependent){
        //     //         item[param] = document.getElementById('add:'+param+':'+row_number);
        //     //     }
        //     //     console.log(item);
        //     //     for(let every in item){
        //     //         item[every].oninput = e => td.innerText = each.method(item);
        //     //     }
        //     //     // document.getElementById('add:'+param+':'+row_number).oninput = each.method();
        //     // }break;
        //     default:
        //         if (each.content) {
        //             let input = document.createElement('input');
        //             input.classList.add('layui-input');
        //             if (each.col_span) {
        //                 let spans = [];
        //                 td.colSpan = each.col_span;
        //                 for (let i = 0; i < each.col_span; i++) {
        //                     spans[i] = order[each.span_group[i]];
        //                 }
        //                 input.placeholder = each.content(spans);
        //             } else input.placeholder = each.content;
        //             if (each.beSubmit) input.name = each.beSubmit;
        //             if (each.required) input.setAttribute('lay-verify', 'required');
        //             if (each.tip) input.onfocus = function () {
        //                 ctrl.layer.tips(each.tip, input, {tips: 3});
        //             };
        //             if (each.default_value !== undefined) {
        //                 input.value = each.default_value;
        //             }
        //             if (['date', 'month', 'year'].includes(each.type)) ctrl.laydate.render({
        //                 elem: input,
        //                 // range: '~',
        //                 type: each.type
        //             });
        //
        //             input.oninput = e => arg.data[each.beSubmit] = input.value;
        //             td.appendChild(input);
        //         }
        // }
        // td.id = "add:" + each.content + ":" + row_number;
        // line.appendChild(td);
    }
    let td = document.createElement('td');
    td.classList.add('layui-form-item');
    td.style.display = 'none';

    let submit = document.createElement('input');
    submit.type = 'submit';
    submit.id = "add:submit:" + row_number;
    submit.style.display = 'none';
    submit.setAttribute('lay-submit', '');
    submit.setAttribute('lay-filter', submit.id);

    arg.row = submit.id;
    td.appendChild(submit);
    line.appendChild(td);
    table.appendChild(line);
    ctrl.form.render();
    submit_args.push(arg);
    console.log(submit_args);

    arg.xhr = new XMLHttpRequest();

    ctrl.form.on('submit(' + arg.row + ')', function (data) {
        console.log(data.field); //当前容器的全部表单字段，名值对形式：{name: value}
        arg.finish = true;
        let param = "";
        for (let key in data.field) {
            param += '&' + key + '=' + data.field[key];
        }
        arg.xhr.open('get', 'paramTrans.jsp?action=add' + param);
        arg.xhr.send();
        return false;
    });

    tasks.push(new Promise((resolve, reject) => {
        arg.xhr.onreadystatechange = function () {
            if (arg.xhr.status === 200 && arg.xhr.readyState === 4) {
                submit_args.splice(submit_args.indexOf(arg), 1);
                resolve(JSON.parse(arg.xhr.responseText));
            }
        };
    }));
}


function delRows(rows) {
    console.log(rows);
    let target = [];
    for (let row of rows) {
        let arg = {method: 'delete', data: {}};
        arg.data.id = row.id.split(':')[0];
        target.push(row.id.split(':')[1]);
        submit_args.push(arg);
    }
    ctrl.layer.msg('对第' + target + '行的删除任务已加入');
}

/**
 *
 * @param {Array<HTMLTableRowElement>} rows
 */
function changeRows(rows) {
    for (let row of rows) {
        let arg = {method: 'edit', data: {}};
        let row_number = row.id.split(':')[1];

        for (let i = 1; i < admin_table.length; i++) {
            let td = row.childNodes[i];
            let tdType = admin_table[i];
            generateTD(tdType, td, row,arg);
        }

        let td = document.createElement('td');
        td.classList.add('layui-form-item');
        td.style.display = 'none';

        let submit = document.createElement('input');
        submit.type = 'submit';
        submit.id = "edit:submit:" + row_number;
        submit.style.display = 'none';
        submit.setAttribute('lay-submit', '');
        submit.setAttribute('lay-filter', submit.id);

        arg.row = submit.id;
        td.appendChild(submit);
        row.appendChild(td);
        ctrl.form.render();
        submit_args.push(arg);
        console.log(submit_args);

        arg.xhr = new XMLHttpRequest();

        ctrl.form.on('submit(' + arg.row + ')', function (data) {
            console.log('data field',arg.data); //当前容器的全部表单字段，名值对形式：{name: value}
            let argParam = "";
            for (let key in arg.data) {
                argParam += '&' + key + '=' + arg.data[key];
            }
            console.log('arg param',argParam);
            arg.finish = true;
            // let param = "";
            // for (let key in arg.data) {
            //     param += '&' + key + '=' + arg.data[key];
            // }
            arg.xhr.open('get', 'paramTrans.jsp?action=edit' + argParam);
            arg.xhr.send();
            return false;
        });

        tasks.push(new Promise((resolve, reject) => {
            arg.xhr.onreadystatechange = function () {
                if (arg.xhr.status === 200 && arg.xhr.readyState === 4) {
                    submit_args.splice(submit_args.indexOf(arg), 1);
                    resolve(JSON.parse(arg.xhr.responseText));
                }
            };
        }));
    }
}

function generateTD(tdType, td, row, arg) {
    let ntd = document.createElement('td');
    let tr = row;
    let row_number;
    if (td.parentElement) row_number = tr.id.split(':')[1];
    else row_number = row.id;
    switch (tdType.type) {
        case 'checkbox': {
            if (td.firstChild){
                td.firstChild.disabled = false;
                ntd = td;
                ntd.firstChild.setAttribute('lay-filter',tdType.beSubmit+':'+row_number);
                if (ntd.firstChild.checked) arg.data[tdType.beSubmit] = 1;
                ctrl.form.on('checkbox('+tdType.beSubmit+':'+row_number+')',function (data) {
                    if (data.othis[0].classList.contains('layui-form-checked')) {
                        arg.data[tdType.beSubmit] = 1;
                    }else arg.data[tdType.beSubmit] = 0;
                });
            }else {
                ntd.classList.add('layui-form');
                let input = document.createElement('input');
                input.type = 'checkbox';
                input.setAttribute('lay-skin', 'primary');
                if (tdType.beSubmit) input.name = tdType.beSubmit;
                if (tdType.default_value) input.value = tdType.default_value;
                ntd.appendChild(input);
            }
        }
            break;
        // case 'functions':{
        //     let item = {};
        //     for(let param of each.dependent){
        //         item[param] = document.getElementById('add:'+param+':'+row_number);
        //     }
        //     console.log(item);
        //     for(let every in item){
        //         item[every].oninput = e => td.innerText = each.method(item);
        //     }
        //     // document.getElementById('add:'+param+':'+row_number).oninput = each.method();
        // }break;
        default:
            if (tdType.content) {
                let input = document.createElement('input');
                input.classList.add('layui-input');
                if (tdType.col_span) {
                    ntd.colSpan = tdType.col_span;
                    let spans = {placeholder: [], value: []};
                    for (let i = 0; i < tdType.col_span; i++) {
                        spans.placeholder[i] = order[tdType.span_group[i]];
                        if (td.parentElement) {
                            spans.value[i] = tr.children[tdType.span_group[i]].innerText;
                            if (i > 0) tr.removeChild(tr.childNodes[tdType.span_group[i]]);
                        }
                    }
                    input.placeholder = tdType.content(spans.placeholder);
                    if (td.parentElement) input.value = tdType.content(spans.value);
                } else {
                    input.value = td.innerText;
                    input.placeholder = tdType.content;
                }
                arg.data[tdType.beSubmit] = input.value;
                if (tdType.beSubmit) input.name = tdType.beSubmit;
                if (tdType.required) input.setAttribute('lay-verify', 'required');
                if (tdType.tip) input.onfocus = function () {
                    ctrl.layer.tips(tdType.tip, input, {tips: 3});
                };
                // if (tdType.default_value !== undefined) {
                //     input.value = tdType.default_value;
                // }
                if (['date', 'month', 'year'].includes(tdType.type)) {
                    console.log(input);
                    ctrl.laydate.render({
                        elem: input,
                        // range: '~',
                        type: tdType.type
                    });
                }

                input.oninput = e => arg.data[tdType.beSubmit] = input.value;
                ntd.id = "edit:" + input.placeholder + ":" + row_number;
                ntd.appendChild(input);
            }
    }
    if (td.parentElement) tr.replaceChild(ntd,td);
    else tr.appendChild(ntd);
}
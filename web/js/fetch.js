function getNext(step, table) {
    table.innerHTML = '';
    page_no += step;
    if (ctrl.layer) ctrl.loading = ctrl.layer.load();
    let args = '';
    if (typeof search_args != "undefined") {
        for (let {key, value} of search_args) {
            args += '&' + key + '=' + value;
        }
    }
    fetch('page.jsp?page_no=' + page_no + args).then(data => data.json()).then(data => {
        transformTable(data);
    });
}

function generateNode(type, baseNode, option) {
    if (type === 'checkbox') {
        baseNode.classList.add('layui-form');
        let input = document.createElement('input');
        input.type = 'checkbox';
        input.setAttribute('lay-skin', 'primary');
        if (option.checkbox==='true') input.checked = true;
        if (option.name) input.name = option.name;
        if (option.value) input.value = option.value;
        baseNode.appendChild(input);
        return baseNode;
    }
}

function transformTable(data) {
    let row_number = 0;
    for (let each of data) {
        let tr = document.createElement('tr');
        let node = [];
        if (identity === 'admin') {
            // let checkbox = document.createElement('td');
            // checkbox.classList.add('layui-form');
            // checkbox.innerHTML = '<input type="checkbox" lay-skin="primary">';
            // tr.appendChild(checkbox);

            for (let e of admin_table) {
                // console.log(each[e.content]);
                let td = document.createElement('td');
                // if (each[e.content]) td.innerText = each[e.content];
                if (e.isNode) {
                    td = generateNode(e.type, td, {checkbox: each[e.content],name:e.beSubmit,value:1});
                    if (e.content === 'chose') {
                        td.firstElementChild.id = 'chose:' + row_number;
                        td.firstElementChild.setAttribute('lay-filter', 'chose:' + row_number);
                        ctrl.form.on('checkbox(chose:' + row_number + ')', function (data) {
                            if (data.othis[0].classList.contains('layui-form-checked')) tr.classList.add('chosen-row');
                            else tr.classList.remove('chosen-row');
                        });
                    } else td.firstElementChild.setAttribute('disabled','');
                    node[admin_table.indexOf(e)] = td;
                }
                if (e.isPrimary) tr.id = each[e.content] + ':' + row_number;
            }
        }
        for (let i = 0; i < order.length; i++) {
            if (node[i]) tr.appendChild(node[i]);
            else {
                let td = document.createElement('td');
                td.innerText = each[order[i]];
                if (each[order[i]] !== undefined) tr.appendChild(td);
            }
        }
        table.appendChild(tr);
        row_number++;
    }
    ctrl.form.render();
    ctrl.layer.close(ctrl.loading);
}
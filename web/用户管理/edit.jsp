<%@ page contentType="application/json;charset=UTF-8" language="java" %>
<%@ page import="javax.servlet.http.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<sql:setDataSource var="snapshot" driver="com.mysql.jdbc.Driver"
                   url="jdbc:mysql://dedsec.club:3306/xiaoqu?useUnicode=true&characterEncoding=utf-8"
                   user="root" password="xiaoqu@1"/>

<c:catch var="error">
<sql:update dataSource="${snapshot}" var="result">
update admin set userControl = ?, ownerControl = ?, complainControl = ?, instrumentControl = ?, carParkControl = ?, chargeControl = ?, buildingControl = ?<sql:param value="${sqlParam.userControl}"/><sql:param value="${sqlParam.ownerControl}"/><sql:param value="${sqlParam.complainControl}"/><sql:param value="${sqlParam.instrumentControl}"/><sql:param value="${sqlParam.carParkControl}"/><sql:param value="${sqlParam.chargeControl}"/><sql:param value="${sqlParam.buildingControl}"/>, userSee = ?, ownerSee = ?, complainSee = ?, instrumentSee = ?, carParkSee = ?, chargeSee = ?, buildingSee = ?<sql:param value="${sqlParam.userSee}"/><sql:param value="${sqlParam.ownerSee}"/><sql:param value="${sqlParam.complainSee}"/><sql:param value="${sqlParam.instrumentSee}"/><sql:param value="${sqlParam.carParkSee}"/><sql:param value="${sqlParam.chargeSee}"/><sql:param value="${sqlParam.buildingSee}"/><c:if test="${sqlParam.useraccount!=null}">, useraccount = ?<sql:param value="${sqlParam.useraccount}"/></c:if><c:if test="${sqlParam.name!=null}">, name = ?<sql:param value="${sqlParam.name}"/></c:if><c:if test="${sqlParam.userpassword!=null}">, userpassword = ?<sql:param value="${sqlParam.userpassword}"/></c:if>where id = ?<sql:param value="${sqlParam.id}"/>;</sql:update>
</c:catch>

<c:choose>
<c:when test='${error!=null}'>
<c:set var="error" value="${error}"/>
<%
    String err = pageContext.getAttribute("error").toString();
    String[] part = err.split(":");
    StringBuilder res = new StringBuilder();
    for(int i = 2; i < part.length; i++) {
        res.append(part[i]).append("|");
    }
%>
<%="{\"status\":\""+res+"\"}"%>
</c:when>
<c:otherwise>{"status":"ok"}</c:otherwise>
</c:choose>
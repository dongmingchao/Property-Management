<%@ page contentType="application/json;charset=UTF-8" language="java" %>
<%@ page import="javax.servlet.http.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<sql:setDataSource var="snapshot" driver="com.mysql.jdbc.Driver"
                   url="jdbc:mysql://dedsec.club:3306/xiaoqu?useUnicode=true&characterEncoding=utf-8"
                   user="root" password="xiaoqu@1"/>

<fmt:parseNumber value="${param.page_no}" var="pageNumber"/>
<sql:query dataSource="${snapshot}" var="result">
SELECT roomNum,serverAddress, buildingArea, usedArea, fixedCondition, singlePrice, totalPrice, selledOut,buyerNum, buyerName, remark from buildingManagement limit ?, 5;
<sql:param value="${pageNumber}"/>
</sql:query>
[
<c:forEach var="row" items="${result.rows}" varStatus="stat">
{
"房间编号": "<c:out value="${row.roomNum}"/>",
"物业地址": "<c:out value="${row.serverAddress}"/>",
"建筑面积": "<c:out value="${row.buildingArea}"/>",
"使用面积": "<c:out value="${row.usedArea}"/>",
"装修情况": "<c:out value="${row.fixedCondition}"/>",
"单价": "<c:out value="${row.singlePrice}"/>",
"总价": "<c:out value="${row.totalPrice}"/>",
"是否售出": "<c:out value="${row.selledOut}"/>",
"买主编号":"<c:out value="${row.buyerNum}"/>",
"买主姓名": "<c:out value="${row.buyerName}"/>",
"备注": "<c:out value="${row.remark}"/>"
}<c:if test="${!stat.last}">,</c:if>
</c:forEach>
]
<%@ page contentType="application/json;charset=UTF-8" language="java" %>
<%@ page import="javax.servlet.http.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<sql:setDataSource var="snapshot" driver="com.mysql.jdbc.Driver"
                   url="jdbc:mysql://dedsec.club:3306/xiaoqu?useUnicode=true&characterEncoding=utf-8"
                   user="root" password="xiaoqu@1"/>

<fmt:parseNumber value="${param.page_no}" var="pageNumber"/>
<sql:query dataSource="${snapshot}" var="result">
SELECT chargeId ,ownerName, serverAddress, ChargeYear, ChargeMonth, ChargeP, ChargeWay, ChargeStandard,PayReal, PayBalance, ChargeTime,ChargeName from charge limit ?,5;
<sql:param value="${pageNumber}"/>

</sql:query>
[
<c:forEach var="row" items="${result.rows}" varStatus="stat">
{
"收费编号": "<c:out value="${row.ChargeId}"/>",
"住户姓名": "<c:out value="${row.ownerName}"/>",
"物业地址": "<c:out value="${row.serverAddress}"/>",
"年份": "<c:out value="${row.ChargeYear}"/>",
"月份": "<c:out value="${row.ChargeMonth}"/>",
<%--"上月数据": "<c:out value="${row.locate}"/>",--%>
<%--"本月数据": "<c:out value="${row.term}"/>",--%>
"收费项目": "<c:out value="${row.ChargeP}"/>",
"收费方式": "<c:out value="${row.ChargeWay}"/>",
"应收金额": "<c:out value="${row.ChargeStandard}"/>",
"已交金额":"<c:out value="${row.PayReal}"/>",
"欠费金额": "<c:out value="${row.PayBalance}"/>",
"缴费日期": "<c:out value="${row.ChargeTime}"/>",
"办理人":"<c:out value="${row.ChargeName}"/>"
}<c:if test="${!stat.last}">,</c:if>
</c:forEach>
]